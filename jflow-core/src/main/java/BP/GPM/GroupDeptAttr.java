package BP.GPM;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;
import java.util.*;

/** 
 权限组Dept
*/
public class GroupDeptAttr
{
	/** 
	 Dept
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 权限组
	*/
	public static final String FK_Group = "FK_Group";
}